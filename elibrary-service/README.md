# Installation Guide

**elibrary-service**

---

- [Installation Guide](#Installation-guide)
  * [[TOC]](#-toc-)
  * [Technology stack](#technology-stack)
  * [Setup local environment](#setup-local-environment)
  * [Setup database](#setup-database)
  * [Useful plugins (Intellij extensions)](#useful-plugins--intellij-extensions-)
  * [How to build the project:](#how-to-build-the-project-)
    + [Clone the project from the repository :](#clone-the-project-from-the-repository--)
    + [Build the project.](#build-the-project)
    + [Run the project.](#run-the-project)
    + [Access the application.](#access-the-application)
  * [Access API documentation](#access-api-documentation)

---

## Technology stack

| Technology | Description | Link to download |
| --- | --- | --- |
| Java 11 | Programming Language | [Link](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) |
| Spring Boot | Backend framework | [Link](https://www.mysql.com/downloads/) |
| Maven 3.6.3 | Build automation tool | [Link](https://maven.apache.org/download.cgi) |
| Apache Tomcat | Web server | |
| MySql 8 | Database | |
| Intellij | IDE | [Link](https://www.jetbrains.com/idea/download/#section=windows) |

---

## Setup local environment
Needs to install `Java 11`, `Maven 3.6.3` or above, `MySql 8` and `Intellij`.

## Setup database
1. Create a database using below SQL script
    - `CREATE DATABASE elibrary;`
2. Create a user using the below SQL script
    - `CREATE USER 'elibrary'@'localhost' IDENTIFIED BY '1qaz2Wsx';`
    - `GRANT ALL ON elibrary.* TO 'elibrary'@'localhost';`


## Useful plugins (Intellij extensions)
    
- `Spring Assistant` - assist in developing spring applications
- `Lombok` - to remove boilerplate codes
- `google-java-format` - to format the code
- `SonarLint` - to check code quality

## How to build the project:
1. ### Clone the project from the repository :
    - [elibrary-service](https://gitlab.com/pabasarajw/my-sample-project/-/tree/main/elibrary-service)

2. ### Build the project.
        - Go to project directory
        - Run command: `mvn clean install`

3. ### Run the project.
- Using IDE:
    - Open project in Intellij
    - Right-click on the main spring boot application class (`ElibraryServiceApplication`) and click on Run.
- Run from terminal ( as spring boot project):
    - Go to the project directory
    - Run this command: `mvn spring-boot:run`

4. ### Access the application.

- Access the deployed application: http://localhost:8084/elibrary-service/

## Access API documentation 

Swagger API documentation can be found here: http://localhost:8084/elibrary-service/api-ui.html

