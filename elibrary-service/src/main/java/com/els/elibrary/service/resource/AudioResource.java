package com.els.elibrary.service.resource;

import com.els.common.service.dto.ApplicationExceptionResponse;
import com.els.common.service.dto.BaseResponse;
import com.els.elibrary.service.dto.request.AudioRequestModel;
import com.els.elibrary.service.dto.response.AudioResponseModel;
import com.els.elibrary.service.service.AudioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

/**
 * This AudioResource class is used to handle request related to audio.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@RestController
@Slf4j
@RequestMapping("/audios")
@CrossOrigin(origins = "http://localhost:4200")
public class AudioResource {

  /** audioService */
  private AudioService audioService;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public AudioResource(AudioService audioService, MessageSource messageSource) {
    this.audioService = audioService;
    this.messageSource = messageSource;
  }

  @Operation(
      summary = "Creates a new audio",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Created a new audio",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid audio request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<AudioResponseModel>> createAudio(
      @RequestBody AudioRequestModel audioRequestModel) {
    AudioResponseModel audioResponseModel = audioService.saveAudio(audioRequestModel);
    BaseResponse<AudioResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.CREATED.value(),
            messageSource.getMessage("audio.created.successfully", null, Locale.getDefault()),
            audioResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
  }

  @Operation(
      summary = "Get audio by Id",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found the audio",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid audio Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Audio not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<AudioResponseModel>> findAudioById(
      @PathVariable("id") final long id) {
    AudioResponseModel audioResponseModel = audioService.findAudioById(id);
    BaseResponse<AudioResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "audio.found.successfully",
                new Object[] {audioResponseModel.getId()},
                Locale.getDefault()),
            audioResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Get all audios",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found audios",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid DataTables Input",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Audios not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<DataTablesOutput<AudioResponseModel>>> findAllAudios(
      DataTablesInput input) {
    DataTablesOutput<AudioResponseModel> audioResponseModelDataTablesOutput =
        audioService.findAllAudios(input);
    BaseResponse<DataTablesOutput<AudioResponseModel>> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage("audios.retrieved.successfully", null, Locale.getDefault()),
            audioResponseModelDataTablesOutput);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Delete audio",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Deleted the audio",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid audio Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Audio not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<BaseResponse<Boolean>> deleteAudio(@PathVariable("id") final long id) {
    boolean isDeleted = audioService.deleteAudio(id);
    BaseResponse<Boolean> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "audio.deleted.successfully", new Object[] {id}, Locale.getDefault()),
            isDeleted);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Update an audio",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Updated the audio",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid audio request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PutMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<AudioResponseModel>> updateAudio(
      @RequestBody AudioRequestModel audioRequestModel) {
    AudioResponseModel audioResponseModel = audioService.updateAudio(audioRequestModel);
    BaseResponse<AudioResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "audio.updated.successfully",
                new Object[] {audioResponseModel.getId()},
                Locale.getDefault()),
            audioResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }
}
