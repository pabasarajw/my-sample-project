package com.els.elibrary.service.entity;

import com.els.common.service.entity.BaseEntity;
import com.els.elibrary.service.enums.ELibraryStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The Video class is used to represent the video entity.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(
    name = "VIDEO",
    indexes = {@Index(name = "video_id_pk_index", columnList = "ID")})
public class Video extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", unique = true, nullable = false)
  private Long id;

  @Column(name = "TITLE", length = 100, nullable = false)
  private String title;

  @Column(name = "STATUS", nullable = false)
  private ELibraryStatus status;

  @Lob
  @Column(name = "DESCRIPTION", nullable = true)
  private byte[] description;

  @Column(name = "PUBLISHED_DATE", nullable = false)
  private LocalDate publishedDate;

  @Column(name = "MAIN_IMAGE_URL", nullable = false)
  private String mainImageUrl;

  @Column(name = "MAIN_IMAGE_NAME", nullable = false)
  private String mainImageName;

  @Column(name = "VIDEO_FILE_URL", nullable = false)
  private String videoFileUrl;

  @Column(name = "VIDEO_FILE_NAME", nullable = false)
  private String videoFileName;

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = CascadeType.ALL,
      orphanRemoval = true,
      mappedBy = "video")
  private Set<VideoAuthor> videoAuthors;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    Video video = (Video) o;
    return Objects.equals(id, video.id)
        && Objects.equals(title, video.title)
        && status == video.status
        && Arrays.equals(description, video.description)
        && Objects.equals(publishedDate, video.publishedDate)
        && Objects.equals(mainImageUrl, video.mainImageUrl)
        && Objects.equals(mainImageName, video.mainImageName)
        && Objects.equals(videoFileUrl, video.videoFileUrl)
        && Objects.equals(videoFileName, video.videoFileName)
        && Objects.equals(videoAuthors, video.videoAuthors);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            super.hashCode(),
            id,
            title,
            status,
            publishedDate,
            mainImageUrl,
            mainImageName,
            videoFileUrl,
            videoFileName,
            videoAuthors);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
