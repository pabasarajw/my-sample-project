package com.els.elibrary.service.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The ELibraryStatus enum is used to define the basic elibrary status: ACTIVE / INACTIVE / DELETED
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public enum ELibraryStatus {
  ACTIVE(0, "Active"),
  INACTIVE(1, "Inactive"),
  DELETED(2, "Deleted");

  private int code;
  private String name;

  private ELibraryStatus(int code, String name) {
    this.code = code;
    this.name = name;
  }

  public int getCode() {
    return code;
  }

  public String getName() {
    return name;
  }

  private static final Map<Integer, ELibraryStatus> LOOKUP = new HashMap<Integer, ELibraryStatus>();

  static {
    for (ELibraryStatus eLibraryStatus : EnumSet.allOf(ELibraryStatus.class)) {
      LOOKUP.put(eLibraryStatus.getCode(), eLibraryStatus);
    }
  }

  public static ELibraryStatus fromCode(int code) {
    return LOOKUP.get(code);
  }
}
