package com.els.elibrary.service.service;

import com.els.common.service.exception.ELSResourceNotFoundException;
import com.els.elibrary.service.dto.converter.EBookAuthorConverter;
import com.els.elibrary.service.dto.converter.EBookConverter;
import com.els.elibrary.service.dto.request.EBookAuthorRequestModel;
import com.els.elibrary.service.dto.request.EBookRequestModel;
import com.els.elibrary.service.dto.response.EBookAuthorResponseModel;
import com.els.elibrary.service.dto.response.EBookResponseModel;
import com.els.elibrary.service.entity.EBook;
import com.els.elibrary.service.entity.EBookAuthor;
import com.els.elibrary.service.repository.EBookAuthorRepository;
import com.els.elibrary.service.repository.EBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * This EBookService class is used to implement logics related to ebook.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Service
public class EBookService {

  private EBookRepository eBookRepository;

  private EBookConverter eBookConverter;

  private EBookAuthorConverter eBookAuthorConverter;

  private EBookAuthorRepository eBookAuthorRepository;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public EBookService(
      EBookRepository eBookRepository,
      EBookConverter eBookConverter,
      EBookAuthorConverter eBookAuthorConverter,
      EBookAuthorRepository eBookAuthorRepository,
      MessageSource messageSource) {
    this.eBookRepository = eBookRepository;
    this.eBookConverter = eBookConverter;
    this.eBookAuthorConverter = eBookAuthorConverter;
    this.eBookAuthorRepository = eBookAuthorRepository;
    this.messageSource = messageSource;
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public EBookResponseModel saveEBook(EBookRequestModel eBookRequestModel) {
    Set<EBookAuthorRequestModel> eBookAuthorRequestModels =
        eBookRequestModel.getEBookAuthorRequestModels();
    Set<EBookAuthorResponseModel> eBookAuthorResponseModels = new HashSet<>();

    EBook eBook = eBookConverter.eBookDtoToEBook(eBookRequestModel);
    if (!CollectionUtils.isEmpty(eBook.getEBookAuthors())) eBook.getEBookAuthors().clear();
    eBook = eBookRepository.save(eBook);

    if (!CollectionUtils.isEmpty(eBookAuthorRequestModels)) {
      eBookAuthorResponseModels = saveEBookAuthors(eBook, eBookAuthorRequestModels);
    }

    EBookResponseModel eBookResponseModel = eBookConverter.eBookToEBookDto(eBook);
    eBookResponseModel.setEBookAuthorResponseModels(eBookAuthorResponseModels);
    return eBookResponseModel;
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public Set<EBookAuthorResponseModel> saveEBookAuthors(
      EBook eBook, Set<EBookAuthorRequestModel> eBookAuthorRequestModels) {
    Set<EBookAuthor> authorSet = new HashSet<>();
    eBookAuthorRequestModels.forEach(
        eBookAuthorReq -> {
          eBookAuthorReq.setId(null);
          EBookAuthor author = eBookAuthorConverter.eBookAuthorDtoToEBookAuthor(eBookAuthorReq);
          author.setEBook(eBook);
          authorSet.add(author);
        });
    List<EBookAuthor> authorList = eBookAuthorRepository.saveAll(authorSet);

    Set<EBookAuthorResponseModel> eBookAuthorResponseModels = new HashSet<>();
    authorList.forEach(
        eBookAuthor ->
            eBookAuthorResponseModels.add(
                eBookAuthorConverter.eBookAuthorToEBookAuthorDto(eBookAuthor)));
    return eBookAuthorResponseModels;
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
  public EBookResponseModel findEBookById(Long id) {
    EBook eBook =
        eBookRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "ebook.not.found", new Object[] {id}, Locale.getDefault())));
    Set<EBookAuthorResponseModel> eBookAuthorResponseModels =
        createEBookAuthorResponseModelSet(eBook.getEBookAuthors());
    EBookResponseModel eBookResponseModel = eBookConverter.eBookToEBookDto(eBook);
    eBookResponseModel.setEBookAuthorResponseModels(eBookAuthorResponseModels);
    return eBookResponseModel;
  }

  private Set<EBookAuthorResponseModel> createEBookAuthorResponseModelSet(
      Set<EBookAuthor> eBookAuthors) {
    Set<EBookAuthorResponseModel> eBookAuthorResponseModels = new HashSet<>();
    if (!CollectionUtils.isEmpty(eBookAuthors)) {
      eBookAuthors.forEach(
          eBookAuthor ->
              eBookAuthorResponseModels.add(
                  eBookAuthorConverter.eBookAuthorToEBookAuthorDto(eBookAuthor)));
    }
    return eBookAuthorResponseModels;
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
  public DataTablesOutput<EBookResponseModel> findAllEBooks(DataTablesInput dataTablesInput) {
    DataTablesOutput<EBook> dataTablesOutput = eBookRepository.findAll(dataTablesInput);
    List<EBook> eBookList = dataTablesOutput.getData();
    List<EBookResponseModel> eBookResponseModelList = new ArrayList<>();

    if (!CollectionUtils.isEmpty(eBookList)) {
      eBookList.forEach(
          eBook -> {
            Set<EBookAuthorResponseModel> eBookAuthorResponseModels =
                createEBookAuthorResponseModelSet(eBook.getEBookAuthors());
            EBookResponseModel eBookResponseModel = eBookConverter.eBookToEBookDto(eBook);
            eBookResponseModel.setEBookAuthorResponseModels(eBookAuthorResponseModels);
            eBookResponseModelList.add(eBookResponseModel);
          });
    }

    DataTablesOutput<EBookResponseModel> outputResp = new DataTablesOutput<>();
    outputResp.setData(eBookResponseModelList);
    outputResp.setDraw(dataTablesOutput.getDraw());
    outputResp.setError(dataTablesOutput.getError());
    outputResp.setRecordsFiltered(dataTablesOutput.getRecordsFiltered());
    outputResp.setRecordsTotal(dataTablesOutput.getRecordsTotal());
    return outputResp;
  }

  @Transactional(readOnly = false)
  public boolean deleteEBook(Long id) {
    EBook eBook =
        eBookRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "ebook.not.found", new Object[] {id}, Locale.getDefault())));
    eBookRepository.delete(eBook);
    return true;
  }

  @Transactional(readOnly = false)
  public EBookResponseModel updateEBook(EBookRequestModel ebookRequestModel) {
    Set<EBookAuthorRequestModel> ebookAuthorRequestModels =
        ebookRequestModel.getEBookAuthorRequestModels();
    Set<EBookAuthorResponseModel> ebookAuthorResponseModels = new HashSet<>();

    EBook ebook =
        eBookRepository
            .findById(ebookRequestModel.getId())
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "ebook.not.found",
                            new Object[] {ebookRequestModel.getId()},
                            Locale.getDefault())));

    if (!CollectionUtils.isEmpty(ebook.getEBookAuthors())) {
      ebook
          .getEBookAuthors()
          .forEach(ebookAuthor -> eBookAuthorRepository.deleteById(ebookAuthor.getId()));
      ebook.getEBookAuthors().clear();
    }

    updateEBookDetails(ebook, ebookRequestModel);
    eBookRepository.save(ebook);

    if (!CollectionUtils.isEmpty(ebookAuthorRequestModels))
      ebookAuthorResponseModels = saveEBookAuthors(ebook, ebookAuthorRequestModels);

    EBookResponseModel ebookResponseModel = eBookConverter.eBookToEBookDto(ebook);
    ebookResponseModel.setEBookAuthorResponseModels(ebookAuthorResponseModels);
    return ebookResponseModel;
  }

  private EBook updateEBookDetails(EBook ebook, EBookRequestModel ebookRequestModel) {
    ebook.setTitle(ebookRequestModel.getTitle());
    ebook.setStatus(ebookRequestModel.getStatus());
    ebook.setDescription(ebookRequestModel.getDescription());
    ebook.setPublishedDate(ebookRequestModel.getPublishedDate());
    ebook.setMainImageName(ebookRequestModel.getMainImageName());
    ebook.setMainImageUrl(ebookRequestModel.getMainImageUrl());
    ebook.setEBookPdfName(ebookRequestModel.getEBookPdfName());
    ebook.setEBookPdfUrl(ebookRequestModel.getEBookPdfUrl());
    return ebook;
  }
}
