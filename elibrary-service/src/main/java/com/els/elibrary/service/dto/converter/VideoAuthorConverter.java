package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.VideoAuthorRequestModel;
import com.els.elibrary.service.dto.response.VideoAuthorResponseModel;
import com.els.elibrary.service.entity.VideoAuthor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The VideoAuthorConverter class is used to convert video author dto class to video author entity
 * class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class VideoAuthorConverter {

  private ModelMapper modelMapper;

  @Autowired
  public VideoAuthorConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public VideoAuthor videoAuthorDtoToVideoAuthor(VideoAuthorRequestModel videoAuthorRequestModel) {
    return this.modelMapper.map(videoAuthorRequestModel, VideoAuthor.class);
  }

  public VideoAuthorResponseModel videoAuthorToVideoAuthorDto(VideoAuthor videoAuthor) {
    return this.modelMapper.map(videoAuthor, VideoAuthorResponseModel.class);
  }
}
