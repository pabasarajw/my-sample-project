package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.AudioRequestModel;
import com.els.elibrary.service.dto.response.AudioResponseModel;
import com.els.elibrary.service.entity.Audio;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The AudioConverter class is used to convert audio dto class to audio entity class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class AudioConverter {

  private ModelMapper modelMapper;

  @Autowired
  public AudioConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public Audio audioDtoToAudio(AudioRequestModel audioRequestModel) {
    return this.modelMapper.map(audioRequestModel, Audio.class);
  }

  public AudioResponseModel audioToAudioDto(Audio audio) {
    return this.modelMapper.map(audio, AudioResponseModel.class);
  }
}
