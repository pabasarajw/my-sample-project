package com.els.elibrary.service.entity;

import com.els.common.service.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The VideoAuthor class is used to represent the video author entity.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(
    name = "VIDEO_AUTHOR",
    indexes = {@Index(name = "videoAuthor_id_pk_index", columnList = "ID")})
public class VideoAuthor extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", unique = true, nullable = false)
  private Long id;

  @Column(name = "AUTHOR_NAME", length = 50, nullable = false)
  private String authorName;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(
      name = "VIDEO_ID",
      foreignKey = @ForeignKey(name = "fk_video_with_author"),
      referencedColumnName = "ID")
  private Video video;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    VideoAuthor that = (VideoAuthor) o;
    return Objects.equals(id, that.id) && Objects.equals(authorName, that.authorName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), id, authorName);
  }
}
