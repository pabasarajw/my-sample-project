package com.els.elibrary.service.dto.response;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The VideoResponseModel class is used to transfer video response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "VideoResponseModel", description = "Represent video response model")
public class VideoResponseModel implements Serializable {

  @Schema(title = "Video id", description = "Auto generated id for video")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Title of the video",
      description = "Min 2 characters & Max allowed 100 characters")
  @JsonProperty("title")
  private String title;

  @Schema(title = "Status of the video", description = "Status are - ACTIVE/ INACTIVE/ DELETED")
  @JsonProperty("status")
  private ELibraryStatus status;

  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location")
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @Schema(title = "Main image file name", description = "Uploaded main image file name")
  @JsonProperty("mainImageName")
  private String mainImageName;

  @Schema(title = "Video file url", description = "Video file url of the uploaded location")
  @JsonProperty("videoFileUrl")
  private String videoFileUrl;

  @Schema(title = "Video file name", description = "Uploaded video file name")
  @JsonProperty("videoFileName")
  private String videoFileName;

  @Schema(title = "Description of the video")
  @Lob
  private byte[] description;

  @Schema(title = "Published date of the video", description = "Date pattern is dd-MM-yyyy")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "Video author response models",
      description = "A set of video author response models")
  @JsonProperty("videoAuthorList")
  private Set<VideoAuthorResponseModel> videoAuthorResponseModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VideoResponseModel that = (VideoResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(videoFileUrl, that.videoFileUrl)
        && Objects.equals(videoFileName, that.videoFileName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(videoAuthorResponseModels, that.videoAuthorResponseModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            videoFileUrl,
            videoFileName,
            publishedDate,
            videoAuthorResponseModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
