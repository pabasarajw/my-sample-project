package com.els.elibrary.service.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The OpenApiDocumentationConfig class is used to define swagger configurations
 *
 * @author pabasara
 * @version 1.0
 * @since 1.0
 */
@Configuration
public class OpenApiDocumentationConfig {
  @Bean
  public OpenAPI customOpenAPI() {
    return new OpenAPI()
        .info(
            new Info()
                .title("eLibrary Service API")
                .version("1.0.0")
                .contact(new Contact().name("Pabasara Wijerathne").email("pabasara8857@gmail.com"))
                .description("APIs for eLibrary service")
                .termsOfService("")
                .license(
                    new License()
                        .name("Apache 2.0")
                        .url("http://www.apache.org/licenses/LICENSE-2.0.html")))
        .addServersItem(
            new Server()
                .url("http://localhost:8080/elibrary-service")
                .description("Local Server"));
  }
}
