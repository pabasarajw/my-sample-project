package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.AudioAuthorRequestModel;
import com.els.elibrary.service.dto.response.AudioAuthorResponseModel;
import com.els.elibrary.service.entity.AudioAuthor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The AudioAuthorConverter class is used to convert audio author dto class to audio author entity
 * class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class AudioAuthorConverter {

  private ModelMapper modelMapper;

  @Autowired
  public AudioAuthorConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public AudioAuthor audioAuthorDtoToAudioAuthor(AudioAuthorRequestModel audioAuthorRequestModel) {
    return this.modelMapper.map(audioAuthorRequestModel, AudioAuthor.class);
  }

  public AudioAuthorResponseModel audioAuthorToAudioAuthorDto(AudioAuthor audioAuthor) {
    return this.modelMapper.map(audioAuthor, AudioAuthorResponseModel.class);
  }
}
