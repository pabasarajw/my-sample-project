package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.ArticleAuthor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This ArticleAuthorRepository class is used to handle database transactions related to article
 * author.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Repository
public interface ArticleAuthorRepository extends JpaRepository<ArticleAuthor, Long> {}
