package com.els.elibrary.service.entity;

import com.els.common.service.entity.BaseEntity;
import com.els.elibrary.service.enums.ELibraryStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The Article class is used to represent the article entity.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(
    name = "ARTICLE",
    indexes = {@Index(name = "article_id_pk_index", columnList = "ID")})
public class Article extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", unique = true, nullable = false)
  private Long id;

  @Column(name = "TITLE", length = 100, nullable = false)
  private String title;

  @Column(name = "STATUS", nullable = false)
  private ELibraryStatus status;

  @Lob
  @Column(name = "DESCRIPTION", nullable = true)
  private byte[] description;

  @Column(name = "PUBLISHED_DATE", nullable = false)
  private LocalDate publishedDate;

  @Column(name = "MAIN_IMAGE_URL", nullable = false)
  private String mainImageUrl;

  @Column(name = "MAIN_IMAGE_NAME", nullable = false)
  private String mainImageName;

  @Column(name = "ARTICLE_PDF_URL", nullable = false)
  private String articlePdfUrl;

  @Column(name = "ARTICLE_PDF_NAME", nullable = false)
  private String articlePdfName;

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = CascadeType.ALL,
      orphanRemoval = true,
      mappedBy = "article")
  private Set<ArticleAuthor> articleAuthors;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    Article article = (Article) o;
    return Objects.equals(id, article.id)
        && Objects.equals(title, article.title)
        && status == article.status
        && Arrays.equals(description, article.description)
        && Objects.equals(publishedDate, article.publishedDate)
        && Objects.equals(mainImageUrl, article.mainImageUrl)
        && Objects.equals(mainImageName, article.mainImageName)
        && Objects.equals(articlePdfUrl, article.articlePdfUrl)
        && Objects.equals(articlePdfName, article.articlePdfName)
        && Objects.equals(articleAuthors, article.articleAuthors);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            super.hashCode(),
            id,
            title,
            status,
            publishedDate,
            mainImageUrl,
            mainImageName,
            articlePdfUrl,
            articlePdfName,
            articleAuthors);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
