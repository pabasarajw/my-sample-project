package com.els.elibrary.service.dto.request;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The VideoRequestModel class is used to transfer video request model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "VideoRequestModel", description = "Represent video request model")
public class VideoRequestModel implements Serializable {

  @Schema(title = "Video id", description = "Auto generated id for video", required = false)
  @JsonProperty("id")
  private Long id;

  @NotNull(message = "Title is required")
  @Schema(
      title = "Title of the video",
      description = "Min 2 characters & Max allowed 100 characters",
      required = true)
  @Size(min = 2, max = 100)
  @JsonProperty("title")
  private String title;

  @NotNull(message = "Status is required")
  @Schema(
      title = "Status of the video",
      description = "Status are - ACTIVE/ INACTIVE/ DELETED",
      required = true)
  @JsonProperty("status")
  private ELibraryStatus status;

  @NotNull(message = "Main image file url is required")
  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location",
      required = true)
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @NotNull(message = "Main image file name is required")
  @Schema(
      title = "Main image file name",
      description = "Uploaded main image file name",
      required = true)
  @JsonProperty("mainImageName")
  private String mainImageName;

  @NotNull(message = "Video file url is required")
  @Schema(
      title = "Video file url",
      description = "Video file url of the uploaded location",
      required = true)
  @JsonProperty("videoFileUrl")
  private String videoFileUrl;

  @NotNull(message = "Video file name is required")
  @Schema(title = "Video file name", description = "Uploaded video file name", required = true)
  @JsonProperty("videoFileName")
  private String videoFileName;

  @Schema(title = "Description of the video", required = false)
  @Lob
  private byte[] description;

  @NotNull(message = "Published date is required")
  @Schema(
      title = "Published date of the video",
      description = "Date pattern is dd-MM-yyyy",
      required = true)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "Video author request models",
      description = "A set of video author request models")
  @JsonProperty("videoAuthorList")
  private Set<VideoAuthorRequestModel> videoAuthorRequestModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VideoRequestModel that = (VideoRequestModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(videoFileUrl, that.videoFileUrl)
        && Objects.equals(videoFileName, that.videoFileName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(videoAuthorRequestModels, that.videoAuthorRequestModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            videoFileUrl,
            videoFileName,
            publishedDate,
            videoAuthorRequestModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
