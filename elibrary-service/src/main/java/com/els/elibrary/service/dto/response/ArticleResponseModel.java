package com.els.elibrary.service.dto.response;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The ArticleResponseModel class is used to transfer article response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "ArticleResponseModel", description = "Represent article response model")
public class ArticleResponseModel implements Serializable {
  @Schema(title = "Article id", description = "Auto generated id for article")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Title of the article",
      description = "Min 2 characters & Max allowed 100 characters")
  @JsonProperty("title")
  private String title;

  @Schema(title = "Status of the article", description = "Status are - ACTIVE/ INACTIVE/ DELETED")
  @JsonProperty("status")
  private ELibraryStatus status;

  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location")
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @Schema(title = "Main image file name", description = "Uploaded main image file name")
  @JsonProperty("mainImageName")
  private String mainImageName;

  @Schema(
      title = "Article Pdf file url",
      description = "Article Pdf file url of the uploaded location")
  @JsonProperty("articlePdfUrl")
  private String articlePdfUrl;

  @Schema(title = "Article Pdf file name", description = "Uploaded article Pdf file name")
  @JsonProperty("articlePdfName")
  private String articlePdfName;

  @Schema(title = "Description of the article")
  @Lob
  private byte[] description;

  @Schema(title = "Published date of the article", description = "Date pattern is dd-MM-yyyy")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "Article author response models",
      description = "A set of article author response models")
  @JsonProperty("articleAuthorList")
  private Set<ArticleAuthorResponseModel> articleAuthorResponseModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ArticleResponseModel that = (ArticleResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(articlePdfUrl, that.articlePdfUrl)
        && Objects.equals(articlePdfName, that.articlePdfName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(articleAuthorResponseModels, that.articleAuthorResponseModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            articlePdfUrl,
            articlePdfName,
            publishedDate,
            articleAuthorResponseModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
