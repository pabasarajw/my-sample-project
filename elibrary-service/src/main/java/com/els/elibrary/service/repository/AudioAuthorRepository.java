package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.AudioAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This AudioAuthorRepository class is used to handle database transactions related to audio author.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface AudioAuthorRepository extends JpaRepository<AudioAuthor, Long> {}
