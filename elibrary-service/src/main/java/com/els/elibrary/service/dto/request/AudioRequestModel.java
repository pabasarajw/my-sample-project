package com.els.elibrary.service.dto.request;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The AudioRequestModel class is used to transfer audio request model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "AudioRequestModel", description = "Represent audio request model")
public class AudioRequestModel implements Serializable {

  @Schema(title = "Audio id", description = "Auto generated id for audio", required = false)
  @JsonProperty("id")
  private Long id;

  @NotNull(message = "Title is required")
  @Schema(
      title = "Title of the audio",
      description = "Min 2 characters & Max allowed 100 characters",
      required = true)
  @Size(min = 2, max = 100)
  @JsonProperty("title")
  private String title;

  @NotNull(message = "Status is required")
  @Schema(
      title = "Status of the audio",
      description = "Status are - ACTIVE/ INACTIVE/ DELETED",
      required = true)
  @JsonProperty("status")
  private ELibraryStatus status;

  @NotNull(message = "Main image file url is required")
  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location",
      required = true)
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @NotNull(message = "Main image file name is required")
  @Schema(
      title = "Main image file name",
      description = "Uploaded main image file name",
      required = true)
  @JsonProperty("mainImageName")
  private String mainImageName;

  @NotNull(message = "Audio file url is required")
  @Schema(
      title = "Audio file url",
      description = "Audio file url of the uploaded location",
      required = true)
  @JsonProperty("audioFileUrl")
  private String audioFileUrl;

  @NotNull(message = "Audio file name is required")
  @Schema(title = "Audio file name", description = "Uploaded audio file name", required = true)
  @JsonProperty("audioFileName")
  private String audioFileName;

  @Schema(title = "Description of the audio", required = false)
  @Lob
  private byte[] description;

  @NotNull(message = "Published date is required")
  @Schema(
      title = "Published date of the audio",
      description = "Date pattern is dd-MM-yyyy",
      required = true)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "Audio author request models",
      description = "A set of audio author request models")
  @JsonProperty("audioAuthorList")
  private Set<AudioAuthorRequestModel> audioAuthorRequestModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AudioRequestModel that = (AudioRequestModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(audioFileUrl, that.audioFileUrl)
        && Objects.equals(audioFileName, that.audioFileName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(audioAuthorRequestModels, that.audioAuthorRequestModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            audioFileUrl,
            audioFileName,
            publishedDate,
            audioAuthorRequestModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
