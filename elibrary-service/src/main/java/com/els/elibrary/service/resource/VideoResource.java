package com.els.elibrary.service.resource;

import com.els.common.service.dto.ApplicationExceptionResponse;
import com.els.common.service.dto.BaseResponse;
import com.els.elibrary.service.dto.request.VideoRequestModel;
import com.els.elibrary.service.dto.response.VideoResponseModel;
import com.els.elibrary.service.service.VideoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

/**
 * This VideoResource class is used to handle request related to video.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@RestController
@Slf4j
@RequestMapping("/videos")
@CrossOrigin(origins = "http://localhost:4200")
public class VideoResource {

  /** videoService */
  private VideoService videoService;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public VideoResource(VideoService videoService, MessageSource messageSource) {
    this.videoService = videoService;
    this.messageSource = messageSource;
  }

  @Operation(
      summary = "Creates a new video",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Created a new video",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid video request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<VideoResponseModel>> createVideo(
      @RequestBody VideoRequestModel videoRequestModel) {
    VideoResponseModel videoResponseModel = videoService.saveVideo(videoRequestModel);
    BaseResponse<VideoResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.CREATED.value(),
            messageSource.getMessage("video.created.successfully", null, Locale.getDefault()),
            videoResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
  }

  @Operation(
      summary = "Get video by Id",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found the video",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid video Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Video not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<VideoResponseModel>> findVideoById(
      @PathVariable("id") final long id) {
    VideoResponseModel videoResponseModel = videoService.findVideoById(id);
    BaseResponse<VideoResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "video.found.successfully",
                new Object[] {videoResponseModel.getId()},
                Locale.getDefault()),
            videoResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Get all videos",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found videos",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid DataTables Input",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Videos not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<DataTablesOutput<VideoResponseModel>>> findAllVideos(
      DataTablesInput input) {
    DataTablesOutput<VideoResponseModel> videoResponseModelDataTablesOutput =
        videoService.findAllVideos(input);
    BaseResponse<DataTablesOutput<VideoResponseModel>> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage("videos.retrieved.successfully", null, Locale.getDefault()),
            videoResponseModelDataTablesOutput);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Delete video",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Deleted the video",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid video Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Video not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<BaseResponse<Boolean>> deleteVideo(@PathVariable("id") final long id) {
    boolean isDeleted = videoService.deleteVideo(id);
    BaseResponse<Boolean> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "video.deleted.successfully", new Object[] {id}, Locale.getDefault()),
            isDeleted);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Update a video",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Updated the video",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid video request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PutMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<VideoResponseModel>> updateVideo(
      @RequestBody VideoRequestModel videoRequestModel) {
    VideoResponseModel videoResponseModel = videoService.updateVideo(videoRequestModel);
    BaseResponse<VideoResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "video.updated.successfully",
                new Object[] {videoResponseModel.getId()},
                Locale.getDefault()),
            videoResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }
}
