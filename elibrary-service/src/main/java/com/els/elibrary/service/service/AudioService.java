package com.els.elibrary.service.service;

import com.els.common.service.exception.ELSResourceNotFoundException;
import com.els.elibrary.service.dto.converter.AudioAuthorConverter;
import com.els.elibrary.service.dto.converter.AudioConverter;
import com.els.elibrary.service.dto.request.AudioAuthorRequestModel;
import com.els.elibrary.service.dto.request.AudioRequestModel;
import com.els.elibrary.service.dto.response.AudioAuthorResponseModel;
import com.els.elibrary.service.dto.response.AudioResponseModel;
import com.els.elibrary.service.entity.Audio;
import com.els.elibrary.service.entity.AudioAuthor;
import com.els.elibrary.service.repository.AudioAuthorRepository;
import com.els.elibrary.service.repository.AudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * This AudioService class is used to implement logics related to audio.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
public class AudioService {

  private AudioRepository audioRepository;

  private AudioConverter audioConverter;

  private AudioAuthorConverter audioAuthorConverter;

  private AudioAuthorRepository audioAuthorRepository;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public AudioService(
      AudioRepository audioRepository,
      AudioConverter audioConverter,
      AudioAuthorConverter audioAuthorConverter,
      AudioAuthorRepository audioAuthorRepository,
      MessageSource messageSource) {
    this.audioRepository = audioRepository;
    this.audioConverter = audioConverter;
    this.audioAuthorConverter = audioAuthorConverter;
    this.audioAuthorRepository = audioAuthorRepository;
    this.messageSource = messageSource;
  }

  @Transactional(readOnly = false)
  public AudioResponseModel saveAudio(AudioRequestModel audioRequestModel) {
    Set<AudioAuthorRequestModel> audioAuthorRequestModels =
        audioRequestModel.getAudioAuthorRequestModels();
    Set<AudioAuthorResponseModel> audioAuthorResponseModels = new HashSet<>();

    Audio audio = audioConverter.audioDtoToAudio(audioRequestModel);
    if (!CollectionUtils.isEmpty(audio.getAudioAuthors())) audio.getAudioAuthors().clear();
    audio = audioRepository.save(audio);

    if (!CollectionUtils.isEmpty(audioAuthorRequestModels)) {
      audioAuthorResponseModels = saveAudioAuthors(audio, audioAuthorRequestModels);
    }

    AudioResponseModel audioResponseModel = audioConverter.audioToAudioDto(audio);
    audioResponseModel.setAudioAuthorResponseModels(audioAuthorResponseModels);
    return audioResponseModel;
  }

  @Transactional(readOnly = false)
  public Set<AudioAuthorResponseModel> saveAudioAuthors(
      Audio audio, Set<AudioAuthorRequestModel> audioAuthorRequestModels) {
    Set<AudioAuthor> authorSet = new HashSet<>();
    audioAuthorRequestModels.forEach(
        audioAuthorReq -> {
          audioAuthorReq.setId(null);
          AudioAuthor author = audioAuthorConverter.audioAuthorDtoToAudioAuthor(audioAuthorReq);
          author.setAudio(audio);
          authorSet.add(author);
        });
    List<AudioAuthor> authorList = audioAuthorRepository.saveAll(authorSet);

    Set<AudioAuthorResponseModel> audioAuthorResponseModels = new HashSet<>();
    authorList.forEach(
        audioAuthor ->
            audioAuthorResponseModels.add(
                audioAuthorConverter.audioAuthorToAudioAuthorDto(audioAuthor)));
    return audioAuthorResponseModels;
  }

  public AudioResponseModel findAudioById(Long id) {
    Audio audio =
        audioRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "audio.not.found", new Object[] {id}, Locale.getDefault())));
    Set<AudioAuthorResponseModel> audioAuthorResponseModels =
        createAudioAuthorResponseModelSet(audio.getAudioAuthors());
    AudioResponseModel audioResponseModel = audioConverter.audioToAudioDto(audio);
    audioResponseModel.setAudioAuthorResponseModels(audioAuthorResponseModels);
    return audioResponseModel;
  }

  private Set<AudioAuthorResponseModel> createAudioAuthorResponseModelSet(
      Set<AudioAuthor> audioAuthors) {
    Set<AudioAuthorResponseModel> audioAuthorResponseModels = new HashSet<>();
    if (!CollectionUtils.isEmpty(audioAuthors)) {
      audioAuthors.forEach(
          audioAuthor ->
              audioAuthorResponseModels.add(
                  audioAuthorConverter.audioAuthorToAudioAuthorDto(audioAuthor)));
    }
    return audioAuthorResponseModels;
  }

  @Transactional(readOnly = true)
  public DataTablesOutput<AudioResponseModel> findAllAudios(DataTablesInput dataTablesInput) {
    DataTablesOutput<Audio> dataTablesOutput = audioRepository.findAll(dataTablesInput);
    List<Audio> audioList = dataTablesOutput.getData();
    List<AudioResponseModel> audioResponseModelList = new ArrayList<>();

    if (!CollectionUtils.isEmpty(audioList)) {
      audioList.forEach(
          audio -> {
            Set<AudioAuthorResponseModel> audioAuthorResponseModels =
                createAudioAuthorResponseModelSet(audio.getAudioAuthors());
            AudioResponseModel audioResponseModel = audioConverter.audioToAudioDto(audio);
            audioResponseModel.setAudioAuthorResponseModels(audioAuthorResponseModels);
            audioResponseModelList.add(audioResponseModel);
          });
    }

    DataTablesOutput<AudioResponseModel> outputResp = new DataTablesOutput<>();
    outputResp.setData(audioResponseModelList);
    outputResp.setDraw(dataTablesOutput.getDraw());
    outputResp.setError(dataTablesOutput.getError());
    outputResp.setRecordsFiltered(dataTablesOutput.getRecordsFiltered());
    outputResp.setRecordsTotal(dataTablesOutput.getRecordsTotal());
    return outputResp;
  }

  @Transactional(readOnly = false)
  public boolean deleteAudio(Long id) {
    Audio audio =
        audioRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "audio.not.found", new Object[] {id}, Locale.getDefault())));
    audioRepository.delete(audio);
    return true;
  }

  @Transactional(readOnly = false)
  public AudioResponseModel updateAudio(AudioRequestModel audioRequestModel) {
    Set<AudioAuthorRequestModel> audioAuthorRequestModels =
        audioRequestModel.getAudioAuthorRequestModels();
    Set<AudioAuthorResponseModel> audioAuthorResponseModels = new HashSet<>();

    Audio audio =
        audioRepository
            .findById(audioRequestModel.getId())
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "audio.not.found",
                            new Object[] {audioRequestModel.getId()},
                            Locale.getDefault())));

    if (!CollectionUtils.isEmpty(audio.getAudioAuthors())) {
      audio
          .getAudioAuthors()
          .forEach(audioAuthor -> audioAuthorRepository.deleteById(audioAuthor.getId()));
      audio.getAudioAuthors().clear();
    }

    updateAudioDetails(audio, audioRequestModel);
    audioRepository.save(audio);

    if (!CollectionUtils.isEmpty(audioAuthorRequestModels))
      audioAuthorResponseModels = saveAudioAuthors(audio, audioAuthorRequestModels);

    AudioResponseModel audioResponseModel = audioConverter.audioToAudioDto(audio);
    audioResponseModel.setAudioAuthorResponseModels(audioAuthorResponseModels);
    return audioResponseModel;
  }

  private Audio updateAudioDetails(Audio audio, AudioRequestModel audioRequestModel) {
    audio.setTitle(audioRequestModel.getTitle());
    audio.setStatus(audioRequestModel.getStatus());
    audio.setDescription(audioRequestModel.getDescription());
    audio.setPublishedDate(audioRequestModel.getPublishedDate());
    audio.setMainImageName(audioRequestModel.getMainImageName());
    audio.setMainImageUrl(audioRequestModel.getMainImageUrl());
    audio.setAudioFileName(audioRequestModel.getAudioFileName());
    audio.setAudioFileUrl(audioRequestModel.getAudioFileUrl());
    return audio;
  }
}
