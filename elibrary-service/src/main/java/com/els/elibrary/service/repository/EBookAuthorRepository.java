package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.EBookAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This EBookAuthorRepository class is used to handle database transactions related to ebook author.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface EBookAuthorRepository extends JpaRepository<EBookAuthor, Long> {}
