package com.els.elibrary.service.dto.request;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The EBookRequestModel class is used to transfer ebook request model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "EBookRequestModel", description = "Represent eBook request model")
public class EBookRequestModel implements Serializable {

  @Schema(title = "EBook id", description = "Auto generated id for eBook", required = false)
  @JsonProperty("id")
  private Long id;

  @NotNull(message = "Title is required")
  @Schema(
      title = "Title of the eBook",
      description = "Min 2 characters & Max allowed 100 characters",
      required = true)
  @Size(min = 2, max = 100)
  @JsonProperty("title")
  private String title;

  @NotNull(message = "Status is required")
  @Schema(
      title = "Status of the eBook",
      description = "Status are - ACTIVE/ INACTIVE/ DELETED",
      required = true)
  @JsonProperty("status")
  private ELibraryStatus status;

  @NotNull(message = "Main image file url is required")
  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location",
      required = true)
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @NotNull(message = "Main image file name is required")
  @Schema(
      title = "Main image file name",
      description = "Uploaded main image file name",
      required = true)
  @JsonProperty("mainImageName")
  private String mainImageName;

  @NotNull(message = "EBook Pdf file url is required")
  @Schema(
      title = "EBook Pdf file url",
      description = "EBook Pdf file url of the uploaded location",
      required = true)
  @JsonProperty("eBookPdfUrl")
  private String eBookPdfUrl;

  @NotNull(message = "EBook Pdf file name is required")
  @Schema(
      title = "EBook Pdf file name",
      description = "Uploaded eBook Pdf file name",
      required = true)
  @JsonProperty("eBookPdfName")
  private String eBookPdfName;

  @Schema(title = "Description of the eBook", required = false)
  @Lob
  private byte[] description;

  @NotNull(message = "Published date is required")
  @Schema(
      title = "Published date of the eBook",
      description = "Date pattern is dd-MM-yyyy",
      required = true)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "EBook author request models",
      description = "A set of eBook author request models")
  @JsonProperty("eBookAuthorList")
  private Set<EBookAuthorRequestModel> eBookAuthorRequestModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EBookRequestModel that = (EBookRequestModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(eBookPdfUrl, that.eBookPdfUrl)
        && Objects.equals(eBookPdfName, that.eBookPdfName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(eBookAuthorRequestModels, that.eBookAuthorRequestModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            eBookPdfUrl,
            eBookPdfName,
            publishedDate,
            eBookAuthorRequestModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
