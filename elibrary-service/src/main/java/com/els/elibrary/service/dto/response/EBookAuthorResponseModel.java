package com.els.elibrary.service.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;

/**
 * The EBookAuthorResponseModel class is used to transfer ebook author response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "EBookAuthorResponseModel", description = "Represent eBook author response model")
public class EBookAuthorResponseModel implements Serializable {

  @Schema(title = "EBook author id", description = "Auto generated id for eBook author")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Name of the author",
      description = "Min 2 characters & Max allowed 50 characters")
  @JsonProperty("authorName")
  private String authorName;

  @Schema(title = "EBook response model")
  @JsonProperty("eBookResponseModel")
  private EBookResponseModel eBookResponseModel;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EBookAuthorResponseModel that = (EBookAuthorResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(authorName, that.authorName)
        && Objects.equals(eBookResponseModel, that.eBookResponseModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, authorName, eBookResponseModel);
  }
}
