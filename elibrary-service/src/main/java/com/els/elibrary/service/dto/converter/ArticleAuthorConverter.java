package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.ArticleAuthorRequestModel;
import com.els.elibrary.service.dto.response.ArticleAuthorResponseModel;
import com.els.elibrary.service.entity.ArticleAuthor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The ArticleAuthorConverter class is used to convert article author dto class to article author
 * entity class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class ArticleAuthorConverter {

  private ModelMapper modelMapper;

  @Autowired
  public ArticleAuthorConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public ArticleAuthor articleAuthorDtoToArticleAuthor(
      ArticleAuthorRequestModel articleAuthorRequestModel) {
    return this.modelMapper.map(articleAuthorRequestModel, ArticleAuthor.class);
  }

  public ArticleAuthorResponseModel articleAuthorToArticleAuthorDto(ArticleAuthor articleAuthor) {
    return this.modelMapper.map(articleAuthor, ArticleAuthorResponseModel.class);
  }
}
