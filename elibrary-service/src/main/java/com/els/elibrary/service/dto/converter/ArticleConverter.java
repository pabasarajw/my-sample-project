package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.ArticleRequestModel;
import com.els.elibrary.service.dto.response.ArticleResponseModel;
import com.els.elibrary.service.entity.Article;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The ArticleConverter class is used to convert article dto class to article entity class and vice
 * versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class ArticleConverter {

  private ModelMapper modelMapper;

  @Autowired
  public ArticleConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public Article articleDtoToArticle(ArticleRequestModel articleRequestModel) {
    return this.modelMapper.map(articleRequestModel, Article.class);
  }

  public ArticleResponseModel articleToArticleDto(Article article) {
    return this.modelMapper.map(article, ArticleResponseModel.class);
  }
}
