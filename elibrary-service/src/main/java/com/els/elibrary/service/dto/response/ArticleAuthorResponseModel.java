package com.els.elibrary.service.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;

/**
 * The ArticleAuthorResponseModel class is used to transfer article author response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(
    title = "ArticleAuthorResponseModel",
    description = "Represent article author response model")
public class ArticleAuthorResponseModel implements Serializable {

  @Schema(title = "Article author id", description = "Auto generated id for article author")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Name of the author",
      description = "Min 2 characters & Max allowed 50 characters")
  @JsonProperty("authorName")
  private String authorName;

  @Schema(title = "Article response model")
  @JsonProperty("articleResponseModel")
  private ArticleResponseModel articleResponseModel;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ArticleAuthorResponseModel that = (ArticleAuthorResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(authorName, that.authorName)
        && Objects.equals(articleResponseModel, that.articleResponseModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, authorName, articleResponseModel);
  }
}
