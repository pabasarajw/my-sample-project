package com.els.elibrary.service.dto.response;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The AudioResponseModel class is used to transfer audio response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "AudioResponseModel", description = "Represent audio response model")
public class AudioResponseModel implements Serializable {

  @Schema(title = "Audio id", description = "Auto generated id for audio")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Title of the audio",
      description = "Min 2 characters & Max allowed 100 characters")
  @JsonProperty("title")
  private String title;

  @Schema(title = "Status of the audio", description = "Status are - ACTIVE/ INACTIVE/ DELETED")
  @JsonProperty("status")
  private ELibraryStatus status;

  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location")
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @Schema(title = "Main image file name", description = "Uploaded main image file name")
  @JsonProperty("mainImageName")
  private String mainImageName;

  @Schema(title = "Audio file url", description = "Audio file url of the uploaded location")
  @JsonProperty("audioFileUrl")
  private String audioFileUrl;

  @Schema(title = "Audio file name", description = "Uploaded audio file name")
  @JsonProperty("audioFileName")
  private String audioFileName;

  @Schema(title = "Description of the audio")
  @Lob
  private byte[] description;

  @Schema(title = "Published date of the audio", description = "Date pattern is dd-MM-yyyy")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "Audio author response models",
      description = "A set of audio author response models")
  @JsonProperty("audioAuthorList")
  private Set<AudioAuthorResponseModel> audioAuthorResponseModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AudioResponseModel that = (AudioResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(audioFileUrl, that.audioFileUrl)
        && Objects.equals(audioFileName, that.audioFileName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(audioAuthorResponseModels, that.audioAuthorResponseModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            audioFileUrl,
            audioFileName,
            publishedDate,
            audioAuthorResponseModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
