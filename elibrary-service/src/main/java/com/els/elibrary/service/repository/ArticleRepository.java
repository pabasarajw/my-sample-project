package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.Article;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

/**
 * This ArticleRepository class is used to handle database transactions related to article .
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface ArticleRepository extends DataTablesRepository<Article, Long> {}
