package com.els.elibrary.service.service;

import com.els.common.service.exception.ELSResourceNotFoundException;
import com.els.elibrary.service.dto.converter.VideoAuthorConverter;
import com.els.elibrary.service.dto.converter.VideoConverter;
import com.els.elibrary.service.dto.request.VideoAuthorRequestModel;
import com.els.elibrary.service.dto.request.VideoRequestModel;
import com.els.elibrary.service.dto.response.VideoAuthorResponseModel;
import com.els.elibrary.service.dto.response.VideoResponseModel;
import com.els.elibrary.service.entity.Video;
import com.els.elibrary.service.entity.VideoAuthor;
import com.els.elibrary.service.repository.VideoAuthorRepository;
import com.els.elibrary.service.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * This VideoService class is used to implement logics related to video.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
public class VideoService {

  private VideoRepository videoRepository;

  private VideoConverter videoConverter;

  private VideoAuthorConverter videoAuthorConverter;

  private VideoAuthorRepository videoAuthorRepository;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public VideoService(
      VideoRepository videoRepository,
      VideoConverter videoConverter,
      VideoAuthorConverter videoAuthorConverter,
      VideoAuthorRepository videoAuthorRepository,
      MessageSource messageSource) {
    this.videoRepository = videoRepository;
    this.videoConverter = videoConverter;
    this.videoAuthorConverter = videoAuthorConverter;
    this.videoAuthorRepository = videoAuthorRepository;
    this.messageSource = messageSource;
  }

  @Transactional(readOnly = false)
  public VideoResponseModel saveVideo(VideoRequestModel videoRequestModel) {
    Set<VideoAuthorRequestModel> videoAuthorRequestModels =
        videoRequestModel.getVideoAuthorRequestModels();
    Set<VideoAuthorResponseModel> videoAuthorResponseModels = new HashSet<>();

    Video video = videoConverter.videoDtoToVideo(videoRequestModel);
    if (!CollectionUtils.isEmpty(video.getVideoAuthors())) video.getVideoAuthors().clear();
    video = videoRepository.save(video);

    if (!CollectionUtils.isEmpty(videoAuthorRequestModels)) {
      videoAuthorResponseModels = saveVideoAuthors(video, videoAuthorRequestModels);
    }

    VideoResponseModel videoResponseModel = videoConverter.videoToVideoDto(video);
    videoResponseModel.setVideoAuthorResponseModels(videoAuthorResponseModels);
    return videoResponseModel;
  }

  @Transactional(readOnly = false)
  public Set<VideoAuthorResponseModel> saveVideoAuthors(
      Video video, Set<VideoAuthorRequestModel> videoAuthorRequestModels) {
    Set<VideoAuthor> authorSet = new HashSet<>();
    videoAuthorRequestModels.forEach(
        videoAuthorReq -> {
          videoAuthorReq.setId(null);
          VideoAuthor author = videoAuthorConverter.videoAuthorDtoToVideoAuthor(videoAuthorReq);
          author.setVideo(video);
          authorSet.add(author);
        });
    List<VideoAuthor> authorList = videoAuthorRepository.saveAll(authorSet);

    Set<VideoAuthorResponseModel> videoAuthorResponseModels = new HashSet<>();
    authorList.forEach(
        videoAuthor ->
            videoAuthorResponseModels.add(
                videoAuthorConverter.videoAuthorToVideoAuthorDto(videoAuthor)));
    return videoAuthorResponseModels;
  }

  public VideoResponseModel findVideoById(Long id) {
    Video video =
        videoRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "video.not.found", new Object[] {id}, Locale.getDefault())));
    Set<VideoAuthorResponseModel> videoAuthorResponseModels =
        createVideoAuthorResponseModelSet(video.getVideoAuthors());
    VideoResponseModel videoResponseModel = videoConverter.videoToVideoDto(video);
    videoResponseModel.setVideoAuthorResponseModels(videoAuthorResponseModels);
    return videoResponseModel;
  }

  private Set<VideoAuthorResponseModel> createVideoAuthorResponseModelSet(
      Set<VideoAuthor> videoAuthors) {
    Set<VideoAuthorResponseModel> videoAuthorResponseModels = new HashSet<>();
    if (!CollectionUtils.isEmpty(videoAuthors)) {
      videoAuthors.forEach(
          videoAuthor ->
              videoAuthorResponseModels.add(
                  videoAuthorConverter.videoAuthorToVideoAuthorDto(videoAuthor)));
    }
    return videoAuthorResponseModels;
  }

  public DataTablesOutput<VideoResponseModel> findAllVideos(DataTablesInput dataTablesInput) {
    DataTablesOutput<Video> dataTablesOutput = videoRepository.findAll(dataTablesInput);
    List<Video> videoList = dataTablesOutput.getData();
    List<VideoResponseModel> videoResponseModelList = new ArrayList<>();

    if (!CollectionUtils.isEmpty(videoList)) {
      videoList.forEach(
          video -> {
            Set<VideoAuthorResponseModel> videoAuthorResponseModels =
                createVideoAuthorResponseModelSet(video.getVideoAuthors());
            VideoResponseModel videoResponseModel = videoConverter.videoToVideoDto(video);
            videoResponseModel.setVideoAuthorResponseModels(videoAuthorResponseModels);
            videoResponseModelList.add(videoResponseModel);
          });
    }

    DataTablesOutput<VideoResponseModel> outputResp = new DataTablesOutput<>();
    outputResp.setData(videoResponseModelList);
    outputResp.setDraw(dataTablesOutput.getDraw());
    outputResp.setError(dataTablesOutput.getError());
    outputResp.setRecordsFiltered(dataTablesOutput.getRecordsFiltered());
    outputResp.setRecordsTotal(dataTablesOutput.getRecordsTotal());
    return outputResp;
  }

  @Transactional(readOnly = false)
  public boolean deleteVideo(Long id) {
    Video video =
        videoRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "video.not.found", new Object[] {id}, Locale.getDefault())));
    videoRepository.delete(video);
    return true;
  }

  @Transactional(readOnly = false)
  public VideoResponseModel updateVideo(VideoRequestModel videoRequestModel) {
    Set<VideoAuthorRequestModel> videoAuthorRequestModels =
        videoRequestModel.getVideoAuthorRequestModels();
    Set<VideoAuthorResponseModel> videoAuthorResponseModels = new HashSet<>();

    Video video =
        videoRepository
            .findById(videoRequestModel.getId())
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "video.not.found",
                            new Object[] {videoRequestModel.getId()},
                            Locale.getDefault())));

    if (!CollectionUtils.isEmpty(video.getVideoAuthors())) {
      video
          .getVideoAuthors()
          .forEach(videoAuthor -> videoAuthorRepository.deleteById(videoAuthor.getId()));
      video.getVideoAuthors().clear();
    }

    updateVideoDetails(video, videoRequestModel);
    videoRepository.save(video);

    if (!CollectionUtils.isEmpty(videoAuthorRequestModels))
      videoAuthorResponseModels = saveVideoAuthors(video, videoAuthorRequestModels);

    VideoResponseModel videoResponseModel = videoConverter.videoToVideoDto(video);
    videoResponseModel.setVideoAuthorResponseModels(videoAuthorResponseModels);
    return videoResponseModel;
  }

  private Video updateVideoDetails(Video video, VideoRequestModel videoRequestModel) {
    video.setTitle(videoRequestModel.getTitle());
    video.setStatus(videoRequestModel.getStatus());
    video.setDescription(videoRequestModel.getDescription());
    video.setPublishedDate(videoRequestModel.getPublishedDate());
    video.setMainImageName(videoRequestModel.getMainImageName());
    video.setMainImageUrl(videoRequestModel.getMainImageUrl());
    video.setVideoFileName(videoRequestModel.getVideoFileName());
    video.setVideoFileUrl(videoRequestModel.getVideoFileUrl());
    return video;
  }
}
