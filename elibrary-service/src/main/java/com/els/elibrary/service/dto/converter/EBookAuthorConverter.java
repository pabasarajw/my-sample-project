package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.EBookAuthorRequestModel;
import com.els.elibrary.service.dto.response.EBookAuthorResponseModel;
import com.els.elibrary.service.entity.EBookAuthor;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The EBookAuthorConverter class is used to convert ebook author dto class to ebook author entity
 * class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class EBookAuthorConverter {

  private ModelMapper modelMapper;

  @Autowired
  public EBookAuthorConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public EBookAuthor eBookAuthorDtoToEBookAuthor(EBookAuthorRequestModel eBookAuthorRequestModel) {
    return this.modelMapper.map(eBookAuthorRequestModel, EBookAuthor.class);
  }

  public EBookAuthorResponseModel eBookAuthorToEBookAuthorDto(EBookAuthor eBookAuthor) {
    return this.modelMapper.map(eBookAuthor, EBookAuthorResponseModel.class);
  }
}
