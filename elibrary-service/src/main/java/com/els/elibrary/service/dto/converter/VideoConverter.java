package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.VideoRequestModel;
import com.els.elibrary.service.dto.response.VideoResponseModel;
import com.els.elibrary.service.entity.Video;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The VideoConverter class is used to convert video dto class to video entity class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class VideoConverter {

  private ModelMapper modelMapper;

  @Autowired
  public VideoConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public Video videoDtoToVideo(VideoRequestModel videoRequestModel) {
    return this.modelMapper.map(videoRequestModel, Video.class);
  }

  public VideoResponseModel videoToVideoDto(Video video) {
    return this.modelMapper.map(video, VideoResponseModel.class);
  }
}
