package com.els.elibrary.service.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * The VideoAuthorRequestModel class is used to transfer video author request model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "VideoAuthorRequestModel", description = "Represent video author request model")
public class VideoAuthorRequestModel implements Serializable {

  @Schema(
      title = "Video author id",
      description = "Auto generated id for video author",
      required = false)
  @JsonProperty("id")
  private Long id;

  @NotNull(message = "Author name is required")
  @Schema(
      title = "Name of the author",
      description = "Min 2 characters & Max allowed 50 characters",
      required = true)
  @Size(min = 2, max = 50)
  @JsonProperty("authorName")
  private String authorName;

  @Schema(title = "Video request model", required = false)
  @JsonProperty("videoRequestModel")
  private VideoRequestModel videoRequestModel;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VideoAuthorRequestModel that = (VideoAuthorRequestModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(authorName, that.authorName)
        && Objects.equals(videoRequestModel, that.videoRequestModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, authorName, videoRequestModel);
  }
}
