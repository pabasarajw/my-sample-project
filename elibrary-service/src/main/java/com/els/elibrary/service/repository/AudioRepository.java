package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.Audio;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

/**
 * This AudioRepository class is used to handle database transactions related to audio.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface AudioRepository extends DataTablesRepository<Audio, Long> {}
