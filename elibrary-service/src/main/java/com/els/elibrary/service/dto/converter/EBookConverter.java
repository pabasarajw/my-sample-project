package com.els.elibrary.service.dto.converter;

import com.els.elibrary.service.dto.request.EBookRequestModel;
import com.els.elibrary.service.dto.response.EBookResponseModel;
import com.els.elibrary.service.entity.EBook;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The EBookConverter class is used to convert ebook dto class to ebook entity
 * class and vice versa.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class EBookConverter {

  private ModelMapper modelMapper;

  @Autowired
  public EBookConverter(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public EBook eBookDtoToEBook(EBookRequestModel eBookRequestModel) {
    return this.modelMapper.map(eBookRequestModel, EBook.class);
  }

  public EBookResponseModel eBookToEBookDto(EBook eBook) {
    return this.modelMapper.map(eBook, EBookResponseModel.class);
  }
}
