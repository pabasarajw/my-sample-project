package com.els.elibrary.service.resource;

import com.els.common.service.dto.ApplicationExceptionResponse;
import com.els.common.service.dto.BaseResponse;
import com.els.elibrary.service.dto.request.EBookRequestModel;
import com.els.elibrary.service.dto.response.EBookResponseModel;
import com.els.elibrary.service.service.EBookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

/**
 * This EBookResource class is used to handle request related to ebook.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@RestController
@Slf4j
@RequestMapping("/ebooks")
@CrossOrigin(origins = "http://localhost:4200")
public class EBookResource {

  /** eBookService */
  private EBookService eBookService;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public EBookResource(EBookService eBookService, MessageSource messageSource) {
    this.eBookService = eBookService;
    this.messageSource = messageSource;
  }

  @Operation(
      summary = "Creates a new ebook",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Created a new ebook",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid ebook request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<EBookResponseModel>> createEBook(
      @RequestBody EBookRequestModel eBookRequestModel) {
    EBookResponseModel eBookResponseModel = eBookService.saveEBook(eBookRequestModel);
    BaseResponse<EBookResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.CREATED.value(),
            messageSource.getMessage("ebook.created.successfully", null, Locale.getDefault()),
            eBookResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
  }

  @Operation(
      summary = "Get ebook by Id",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found the ebook",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid ebook Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "ebook not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<EBookResponseModel>> findEBookById(
      @PathVariable("id") final long id) {
    EBookResponseModel eBookResponseModel = eBookService.findEBookById(id);
    BaseResponse<EBookResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "ebook.found.successfully",
                new Object[] {eBookResponseModel.getId()},
                Locale.getDefault()),
            eBookResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Get all ebooks",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found ebooks",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid DataTables Input",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "ebooks not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<DataTablesOutput<EBookResponseModel>>> findAllEBooks(
      DataTablesInput input) {
    DataTablesOutput<EBookResponseModel> eBookResponseModelDataTablesOutput =
        eBookService.findAllEBooks(input);
    BaseResponse<DataTablesOutput<EBookResponseModel>> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage("ebooks.retrieved.successfully", null, Locale.getDefault()),
            eBookResponseModelDataTablesOutput);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Delete ebook",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Deleted the ebook",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid ebook Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "ebook not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<BaseResponse<Boolean>> deleteEbook(@PathVariable("id") final long id) {
    boolean isDeleted = eBookService.deleteEBook(id);
    BaseResponse<Boolean> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "ebook.deleted.successfully", new Object[] {id}, Locale.getDefault()),
            isDeleted);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Update an ebook",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Updated the ebook",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid ebook request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PutMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<EBookResponseModel>> updateEBook(
      @RequestBody EBookRequestModel ebookRequestModel) {
    EBookResponseModel ebookResponseModel = eBookService.updateEBook(ebookRequestModel);
    BaseResponse<EBookResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "ebook.updated.successfully",
                new Object[] {ebookResponseModel.getId()},
                Locale.getDefault()),
            ebookResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }
}
