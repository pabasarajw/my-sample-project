package com.els.elibrary.service.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.datatables.repository.DataTablesRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The DataTablesConfiguration class is used to enable data tables repository
 *
 * @author pabasara
 * @version 1.0
 * @since 1.0
 */
@Configuration
@EnableJpaRepositories(
    repositoryFactoryBeanClass = DataTablesRepositoryFactoryBean.class,
    basePackages = {"com.els.elibrary.service.repository"})
public class DataTablesConfiguration {}
