package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.VideoAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This VideoAuthorRepository class is used to handle database transactions related to video author.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface VideoAuthorRepository extends JpaRepository<VideoAuthor, Long> {}
