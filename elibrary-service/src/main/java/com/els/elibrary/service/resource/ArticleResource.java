package com.els.elibrary.service.resource;

import com.els.common.service.dto.ApplicationExceptionResponse;
import com.els.common.service.dto.BaseResponse;
import com.els.elibrary.service.dto.request.ArticleRequestModel;
import com.els.elibrary.service.dto.response.ArticleResponseModel;
import com.els.elibrary.service.service.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

/**
 * This ArticleResource class is used to handle request related to article.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@RestController
@Slf4j
@RequestMapping("/articles")
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleResource {

  /** articleService */
  private ArticleService articleService;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public ArticleResource(ArticleService articleService, MessageSource messageSource) {
    this.articleService = articleService;
    this.messageSource = messageSource;
  }

  @Operation(
      summary = "Creates a new article",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Created a new article",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid article request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<ArticleResponseModel>> createArticle(
      @RequestBody ArticleRequestModel articleRequestModel) {
    ArticleResponseModel articleResponseModel = articleService.saveArticle(articleRequestModel);
    BaseResponse<ArticleResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.CREATED.value(),
            messageSource.getMessage("article.created.successfully", null, Locale.getDefault()),
            articleResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
  }

  @Operation(
      summary = "Get article by Id",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found the article",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid article Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Article not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<ArticleResponseModel>> findArticleById(
      @PathVariable("id") final long id) {
    ArticleResponseModel articleResponseModel = articleService.findArticleById(id);
    BaseResponse<ArticleResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "article.found.successfully",
                new Object[] {articleResponseModel.getId()},
                Locale.getDefault()),
            articleResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Get all articles",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found articles",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid DataTables Input",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Articles not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<DataTablesOutput<ArticleResponseModel>>> findAllArticles(
      DataTablesInput input) {
    DataTablesOutput<ArticleResponseModel> articleResponseModelDataTablesOutput =
        articleService.findAllArticles(input);
    BaseResponse<DataTablesOutput<ArticleResponseModel>> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage("articles.retrieved.successfully", null, Locale.getDefault()),
            articleResponseModelDataTablesOutput);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Delete article",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Deleted the article",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid article Id",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Article not found",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<BaseResponse<Boolean>> deleteArticle(@PathVariable("id") final long id) {
    boolean isDeleted = articleService.deleteArticle(id);
    BaseResponse<Boolean> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "article.deleted.successfully", new Object[] {id}, Locale.getDefault()),
            isDeleted);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }

  @Operation(
      summary = "Update an article",
      security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER"))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Updated the article",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = BaseResponse.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid article request",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ApplicationExceptionResponse.class))
            })
      })
  @PutMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BaseResponse<ArticleResponseModel>> updateArticle(
      @RequestBody ArticleRequestModel articleRequestModel) {
    ArticleResponseModel articleResponseModel = articleService.updateArticle(articleRequestModel);
    BaseResponse<ArticleResponseModel> baseResponse =
        new BaseResponse<>(
            HttpStatus.OK.value(),
            messageSource.getMessage(
                "article.updated.successfully",
                new Object[] {articleResponseModel.getId()},
                Locale.getDefault()),
            articleResponseModel);
    return new ResponseEntity<>(baseResponse, HttpStatus.OK);
  }
}
