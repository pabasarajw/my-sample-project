package com.els.elibrary.service.service;

import com.els.common.service.exception.ELSResourceNotFoundException;
import com.els.elibrary.service.dto.converter.ArticleAuthorConverter;
import com.els.elibrary.service.dto.converter.ArticleConverter;
import com.els.elibrary.service.dto.request.ArticleAuthorRequestModel;
import com.els.elibrary.service.dto.request.ArticleRequestModel;
import com.els.elibrary.service.dto.response.ArticleAuthorResponseModel;
import com.els.elibrary.service.dto.response.ArticleResponseModel;
import com.els.elibrary.service.entity.Article;
import com.els.elibrary.service.entity.ArticleAuthor;
import com.els.elibrary.service.repository.ArticleAuthorRepository;
import com.els.elibrary.service.repository.ArticleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * This ArticleService class is used to implement logics related to article.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Service
@Slf4j
@Transactional(propagation = Propagation.REQUIRED)
public class ArticleService {

  private ArticleRepository articleRepository;

  private ArticleConverter articleConverter;

  private ArticleAuthorConverter articleAuthorConverter;

  private ArticleAuthorRepository articleAuthorRepository;

  /** messageSource */
  private MessageSource messageSource;

  @Autowired
  public ArticleService(
      ArticleRepository articleRepository,
      ArticleConverter articleConverter,
      ArticleAuthorConverter articleAuthorConverter,
      ArticleAuthorRepository articleAuthorRepository,
      MessageSource messageSource) {
    this.articleRepository = articleRepository;
    this.articleConverter = articleConverter;
    this.articleAuthorConverter = articleAuthorConverter;
    this.articleAuthorRepository = articleAuthorRepository;
    this.messageSource = messageSource;
  }

  @Transactional(readOnly = false)
  public ArticleResponseModel saveArticle(ArticleRequestModel articleRequestModel) {
    Set<ArticleAuthorRequestModel> articleAuthorRequestModels =
        articleRequestModel.getArticleAuthorRequestModels();
    Set<ArticleAuthorResponseModel> articleAuthorResponseModels = new HashSet<>();

    Article article = articleConverter.articleDtoToArticle(articleRequestModel);
    if (!CollectionUtils.isEmpty(article.getArticleAuthors())) article.getArticleAuthors().clear();
    article = articleRepository.save(article);

    if (!CollectionUtils.isEmpty(articleAuthorRequestModels)) {
      articleAuthorResponseModels = saveArticleAuthors(article, articleAuthorRequestModels);
    }

    ArticleResponseModel articleResponseModel = articleConverter.articleToArticleDto(article);
    articleResponseModel.setArticleAuthorResponseModels(articleAuthorResponseModels);
    return articleResponseModel;
  }

  @Transactional(readOnly = false)
  public Set<ArticleAuthorResponseModel> saveArticleAuthors(
      Article article, Set<ArticleAuthorRequestModel> articleAuthorRequestModels) {
    Set<ArticleAuthor> authorSet = new HashSet<>();
    articleAuthorRequestModels.forEach(
        articleAuthorReq -> {
          articleAuthorReq.setId(null);
          ArticleAuthor author =
              articleAuthorConverter.articleAuthorDtoToArticleAuthor(articleAuthorReq);
          author.setArticle(article);
          authorSet.add(author);
        });
    List<ArticleAuthor> authorList = articleAuthorRepository.saveAll(authorSet);

    Set<ArticleAuthorResponseModel> articleAuthorResponseModels = new HashSet<>();
    authorList.forEach(
        authorAuthor ->
            articleAuthorResponseModels.add(
                articleAuthorConverter.articleAuthorToArticleAuthorDto(authorAuthor)));
    return articleAuthorResponseModels;
  }

  @Transactional(readOnly = true)
  public ArticleResponseModel findArticleById(Long id) {
    Article article =
        articleRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "article.not.found", new Object[] {id}, Locale.getDefault())));
    Set<ArticleAuthorResponseModel> articleAuthorResponseModels =
        createArticleAuthorResponseModelSet(article.getArticleAuthors());
    ArticleResponseModel articleResponseModel = articleConverter.articleToArticleDto(article);
    articleResponseModel.setArticleAuthorResponseModels(articleAuthorResponseModels);
    return articleResponseModel;
  }

  private Set<ArticleAuthorResponseModel> createArticleAuthorResponseModelSet(
      Set<ArticleAuthor> articleAuthors) {
    Set<ArticleAuthorResponseModel> articleAuthorResponseModels = new HashSet<>();
    if (!CollectionUtils.isEmpty(articleAuthors)) {
      articleAuthors.forEach(
          articleAuthor ->
              articleAuthorResponseModels.add(
                  articleAuthorConverter.articleAuthorToArticleAuthorDto(articleAuthor)));
    }
    return articleAuthorResponseModels;
  }

  @Transactional(readOnly = true)
  public DataTablesOutput<ArticleResponseModel> findAllArticles(DataTablesInput dataTablesInput) {
    DataTablesOutput<Article> dataTablesOutput = articleRepository.findAll(dataTablesInput);
    List<Article> articleList = dataTablesOutput.getData();
    List<ArticleResponseModel> articleResponseModelList = new ArrayList<>();

    if (!CollectionUtils.isEmpty(articleList)) {
      articleList.forEach(
          article -> {
            Set<ArticleAuthorResponseModel> articleAuthorResponseModels =
                createArticleAuthorResponseModelSet(article.getArticleAuthors());
            ArticleResponseModel articleResponseModel =
                articleConverter.articleToArticleDto(article);
            articleResponseModel.setArticleAuthorResponseModels(articleAuthorResponseModels);
            articleResponseModelList.add(articleResponseModel);
          });
    }

    DataTablesOutput<ArticleResponseModel> outputResp = new DataTablesOutput<>();
    outputResp.setData(articleResponseModelList);
    outputResp.setDraw(dataTablesOutput.getDraw());
    outputResp.setError(dataTablesOutput.getError());
    outputResp.setRecordsFiltered(dataTablesOutput.getRecordsFiltered());
    outputResp.setRecordsTotal(dataTablesOutput.getRecordsTotal());
    return outputResp;
  }

  @Transactional(readOnly = false)
  public boolean deleteArticle(Long id) {
    Article article =
        articleRepository
            .findById(id)
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "article.not.found", new Object[] {id}, Locale.getDefault())));
    articleRepository.delete(article);
    return true;
  }

  @Transactional(readOnly = false)
  public ArticleResponseModel updateArticle(ArticleRequestModel articleRequestModel) {
    Set<ArticleAuthorRequestModel> articleAuthorRequestModels =
        articleRequestModel.getArticleAuthorRequestModels();
    Set<ArticleAuthorResponseModel> articleAuthorResponseModels = new HashSet<>();

    Article article =
        articleRepository
            .findById(articleRequestModel.getId())
            .orElseThrow(
                () ->
                    new ELSResourceNotFoundException(
                        messageSource.getMessage(
                            "article.not.found",
                            new Object[] {articleRequestModel.getId()},
                            Locale.getDefault())));

    if (!CollectionUtils.isEmpty(article.getArticleAuthors())) {
      article
          .getArticleAuthors()
          .forEach(articleAuthor -> articleAuthorRepository.deleteById(articleAuthor.getId()));
      article.getArticleAuthors().clear();
    }

    updateArticleDetails(article, articleRequestModel);
    articleRepository.save(article);

    if (!CollectionUtils.isEmpty(articleAuthorRequestModels))
      articleAuthorResponseModels = saveArticleAuthors(article, articleAuthorRequestModels);

    ArticleResponseModel articleResponseModel = articleConverter.articleToArticleDto(article);
    articleResponseModel.setArticleAuthorResponseModels(articleAuthorResponseModels);
    return articleResponseModel;
  }

  private Article updateArticleDetails(Article article, ArticleRequestModel articleRequestModel) {
    article.setTitle(articleRequestModel.getTitle());
    article.setStatus(articleRequestModel.getStatus());
    article.setDescription(articleRequestModel.getDescription());
    article.setPublishedDate(articleRequestModel.getPublishedDate());
    article.setMainImageName(articleRequestModel.getMainImageName());
    article.setMainImageUrl(articleRequestModel.getMainImageUrl());
    article.setArticlePdfName(articleRequestModel.getArticlePdfName());
    article.setArticlePdfUrl(articleRequestModel.getArticlePdfUrl());
    return article;
  }
}
