package com.els.elibrary.service.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/***
 * This class is used to define logging aspects.
 * @author pabasara
 * @since 1.0
 * @version 1.0
 */
@Aspect
@Component
@Slf4j
public class LoggingAspect {

  /** Object Mapper */
  private ObjectMapper objectMapper;

  /**
   * This constructor is used to initialize instances created in LoggingAspect class.
   *
   * @param objectMapper ObjectMapper Instance.
   * @author pabasara
   * @since 1.0
   */
  @Autowired
  public LoggingAspect(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * This method is used to define service package pointcut.
   *
   * @author pabasara
   * @since 1.0
   */
  @Pointcut("within(com.els.elibrary.service.service..*)")
  public void servicePackagePointcut() {
    // This is the pointcut for service package
  }

  /**
   * This method is used to define resource package pointcut.
   *
   * @author pabasara
   * @since 1.0
   */
  @Pointcut("within(com.els.elibrary.service.resource..*)")
  public void resourcePackagePointcut() {
    // This is the pointcut for resource package
  }

  /**
   * This method is used to define main package pointcut.
   *
   * @author pabasara
   * @since 1.0
   */
  @Pointcut("within(com.els.elibrary.service..*)")
  public void mainPackagePointcut() {
    // This is the pointcut for main package
  }

  /**
   * This method is used to add method level logs to service package and resource package classes.
   *
   * @param proceedingJoinPoint ProceedingJoinPoint object.
   * @return Object
   * @author pabasara
   * @since 1.0
   */
  @Around("resourcePackagePointcut() || servicePackagePointcut()")
  public Object applicationLogger(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    String methodName = proceedingJoinPoint.getSignature().getName();
    String className = proceedingJoinPoint.getTarget().getClass().toString();
    Object[] requestParams = proceedingJoinPoint.getArgs();
    log.info(
        "Method invoked "
            + className
            + " : "
            + methodName
            + "() "
            + "arguments : "
            + objectMapper.writeValueAsString(requestParams));

    Object responseObject = proceedingJoinPoint.proceed();
    log.info(
        "Method end "
            + className
            + " : "
            + methodName
            + "() "
            + "result : "
            + objectMapper.writeValueAsString(responseObject));
    return responseObject;
  }

  /**
   * This method is used to add method level exception logs to main package classes.
   *
   * @param joinPoint JoinPoint object.
   * @param e Throwable object.
   * @author pabasara
   * @since 1.0
   */
  @AfterThrowing(pointcut = "mainPackagePointcut()", throwing = "e")
  public void exceptionLogger(JoinPoint joinPoint, Throwable e) {
    log.error(
        "Exception in {}.{}() with cause = {}",
        joinPoint.getSignature().getDeclaringTypeName(),
        joinPoint.getSignature().getName(),
        e.getMessage() != null ? e.getMessage() : "NULL");
  }
}
