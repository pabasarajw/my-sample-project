package com.els.elibrary.service.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;

/**
 * The AudioAuthorResponseModel class is used to transfer audio author response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "AudioAuthorResponseModel", description = "Represent audio author response model")
public class AudioAuthorResponseModel implements Serializable {

  @Schema(title = "Audio author id", description = "Auto generated id for audio author")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Name of the author",
      description = "Min 2 characters & Max allowed 50 characters")
  @JsonProperty("authorName")
  private String authorName;

  @Schema(title = "Audio response model")
  @JsonProperty("audioResponseModel")
  private AudioResponseModel audioResponseModel;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AudioAuthorResponseModel that = (AudioAuthorResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(authorName, that.authorName)
        && Objects.equals(audioResponseModel, that.audioResponseModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, authorName, audioResponseModel);
  }
}
