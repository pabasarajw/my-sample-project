package com.els.elibrary.service.repository;

import com.els.elibrary.service.entity.EBook;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

/**
 * This EBookRepository class is used to handle database transactions related to ebook.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface EBookRepository extends DataTablesRepository<EBook, Long> {}
