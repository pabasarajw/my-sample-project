package com.els.elibrary.service;

import com.els.common.service.config.ModelMapperConfig;
import com.els.common.service.exception.ApplicationResponseExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({ApplicationResponseExceptionHandler.class, ModelMapperConfig.class})
@SpringBootApplication
public class ElibraryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ElibraryServiceApplication.class, args);
  }
}
