package com.els.elibrary.service.dto.response;

import com.els.elibrary.service.enums.ELibraryStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * The EBookResponseModel class is used to transfer ebook response model data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Data
@Schema(title = "EBookResponseModel", description = "Represent eBook response model")
public class EBookResponseModel implements Serializable {

  @Schema(title = "EBook id", description = "Auto generated id for eBook")
  @JsonProperty("id")
  private Long id;

  @Schema(
      title = "Title of the eBook",
      description = "Min 2 characters & Max allowed 100 characters")
  @JsonProperty("title")
  private String title;

  @Schema(title = "Status of the eBook", description = "Status are - ACTIVE/ INACTIVE/ DELETED")
  @JsonProperty("status")
  private ELibraryStatus status;

  @Schema(
      title = "Main image file url",
      description = "Main image file url of the uploaded location")
  @JsonProperty("mainImageUrl")
  private String mainImageUrl;

  @Schema(title = "Main image file name", description = "Uploaded main image file name")
  @JsonProperty("mainImageName")
  private String mainImageName;

  @Schema(title = "EBook Pdf file url", description = "EBook Pdf file url of the uploaded location")
  @JsonProperty("eBookPdfUrl")
  private String eBookPdfUrl;

  @Schema(title = "EBook Pdf file name", description = "Uploaded eBook Pdf file name")
  @JsonProperty("eBookPdfName")
  private String eBookPdfName;

  @Schema(title = "Description of the eBook")
  @Lob
  private byte[] description;

  @Schema(title = "Published date of the eBook", description = "Date pattern is dd-MM-yyyy")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate publishedDate;

  @Schema(
      title = "EBook author response models",
      description = "A set of eBook author response models")
  @JsonProperty("eBookAuthorList")
  private Set<EBookAuthorResponseModel> eBookAuthorResponseModels;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EBookResponseModel that = (EBookResponseModel) o;
    return Objects.equals(id, that.id)
        && Objects.equals(title, that.title)
        && status == that.status
        && Objects.equals(mainImageUrl, that.mainImageUrl)
        && Objects.equals(mainImageName, that.mainImageName)
        && Objects.equals(eBookPdfUrl, that.eBookPdfUrl)
        && Objects.equals(eBookPdfName, that.eBookPdfName)
        && Arrays.equals(description, that.description)
        && Objects.equals(publishedDate, that.publishedDate)
        && Objects.equals(eBookAuthorResponseModels, that.eBookAuthorResponseModels);
  }

  @Override
  public int hashCode() {
    int result =
        Objects.hash(
            id,
            title,
            status,
            mainImageUrl,
            mainImageName,
            eBookPdfUrl,
            eBookPdfName,
            publishedDate,
            eBookAuthorResponseModels);
    result = 31 * result + Arrays.hashCode(description);
    return result;
  }
}
