# Installation Guide

**common-service**

---

- [Installation Guide](#Installation-guide)
  * [[TOC]](#-toc-)
  * [Technology stack](#technology-stack)
  * [Setup local environment](#setup-local-environment)
  * [How to build the project:](#how-to-build-the-project-)

---

## Technology stack

| Technology | Description | Link to download |
| --- | --- | --- |
| Java 11 | Programming Language | [Link](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) |
| Maven 3.6.3 | Build automation tool | [Link](https://maven.apache.org/download.cgi) |

---

## Setup local environment
Needs to install `Java` and `Maven 3.6.3` or above.

## How to build the project:
1. ### Clone the project from the repository :
    - [common-service](https://gitlab.com/pabasarajw/my-sample-project/-/tree/main/common-service)

2. ### Build the project.
        - Go to project directory
        - Run command: `mvn clean install`


