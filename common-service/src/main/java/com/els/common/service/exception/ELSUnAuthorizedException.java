package com.els.common.service.exception;

/**
 * The ELSUnAuthorizedException class will be used to throw exception when keycloak returns 401
 * status.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public class ELSUnAuthorizedException extends RuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = -3098669988509514800L;

  /** module error code */
  private String moduleErrorCode;

  /**
   * This constructor is used to initialize message.
   *
   * @param message String
   * @author Pabasara
   * @since 1.0
   */
  public ELSUnAuthorizedException(String message) {
    super(message);
  }

  /**
   * This constructor is used to initialize message and cause.
   *
   * @param message String
   * @param cause cause for the exception
   * @author Pabasara
   * @since 1.0
   */
  public ELSUnAuthorizedException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * This constructor is used to initialize message and moduleErrorCode.
   *
   * @param message String
   * @param moduleErrorCode message specified module error code
   * @author Pabasara
   * @since 1.0
   */
  public ELSUnAuthorizedException(String message, String moduleErrorCode) {
    super(message);
    this.moduleErrorCode = moduleErrorCode;
  }

  /**
   * Gets module error code.
   *
   * @return moduleErrorCode String
   * @author Pabasara
   * @since 1.0
   */
  public String getModuleErrorCode() {
    return moduleErrorCode;
  }
}
