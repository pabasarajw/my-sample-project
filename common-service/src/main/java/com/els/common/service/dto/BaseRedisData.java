package com.els.common.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * This class is used as base module for redis data.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseRedisData<T> implements Serializable {
  /** T value - value to save in Redis */
  private T value;
}
