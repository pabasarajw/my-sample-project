package com.els.common.service.exception;

import com.els.common.service.constants.ApplicationConstants;
import com.els.common.service.dto.ApplicationExceptionResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The ApplicationResponseExceptionHandler class will be used to handle exceptions occurring in
 * application.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@RestControllerAdvice
public class ApplicationResponseExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * This method is used to handle resource not found exception
   *
   * @param ex ELSResourceNotFoundException
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for resource not found
   * @author Pabasara
   * @since 1.0
   */
  @ExceptionHandler(ELSResourceNotFoundException.class)
  public final @ResponseBody ResponseEntity<ApplicationExceptionResponse>
      handleItemNotFoundExceptionException(ELSResourceNotFoundException ex, WebRequest request) {
    Optional<String> optionalErrorMessage = Optional.of(ex.getLocalizedMessage());
    String errorMessage =
        optionalErrorMessage.orElse(ApplicationConstants.RESOURCE_NOT_FOUND_ERROR_MESSAGE);

    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            errorMessage,
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.NOT_FOUND.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  /**
   * This method is used to handle ELSMethodArgumentNotValidException exception
   *
   * @param ex ELSMethodArgumentNotValidException
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for resource not found
   * @author Pabasara
   * @since 1.0
   */
  @ExceptionHandler(ELSMethodArgumentNotValidException.class)
  public final @ResponseBody ResponseEntity<ApplicationExceptionResponse>
      handleELSMethodArgumentNotValidException(
          ELSMethodArgumentNotValidException ex, WebRequest request) {
    Optional<String> optionalErrorMessage = Optional.of(ex.getLocalizedMessage());
    String errorMessage =
        optionalErrorMessage.orElse(ApplicationConstants.BAD_REQUEST_ERROR_MESSAGE);

    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            errorMessage,
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.BAD_REQUEST.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method is used to handle method argument type mismatch exception
   *
   * @param ex MethodArgumentTypeMismatchException
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for method argument type mismatch
   * @author Pabasara
   * @since 1.0
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public final @ResponseBody ResponseEntity<ApplicationExceptionResponse>
      handleMethodArgumentTypeMismatchException(
          MethodArgumentTypeMismatchException ex, WebRequest request) {
    Optional<String> optionalErrorMessage = Optional.of(ex.getLocalizedMessage());
    String errorMessage = optionalErrorMessage.get();
    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            errorMessage,
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.BAD_REQUEST.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method is used to handle un-authorized exception.
   *
   * @param ex ELSUnAuthorizedException
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for ELSUnAuthorizedException
   * @author Pabasara
   * @since 1.0
   */
  @ExceptionHandler(ELSUnAuthorizedException.class)
  public final @ResponseBody ResponseEntity<ApplicationExceptionResponse>
      handleAgentQualificationStatusException(ELSUnAuthorizedException ex, WebRequest request) {

    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            ex.getMessage(),
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.UNAUTHORIZED.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.UNAUTHORIZED);
  }

  /**
   * This method is used to handle all Exceptions
   *
   * @param ex Exception
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for internal server error
   * @author Pabasara
   * @since 1.0
   */
  @ExceptionHandler(ELSException.class)
  public final @ResponseBody ResponseEntity<ApplicationExceptionResponse> handleAllException(
      ELSException ex, WebRequest request) {
    Optional<String> optionalErrorMessage = Optional.of(ex.getLocalizedMessage());
    String errorMessage = optionalErrorMessage.orElse(ApplicationConstants.UNKNOWN_ERROR_MESSAGE);

    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            errorMessage,
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.INTERNAL_SERVER_ERROR.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * This method is used to handle method argument not valid exception
   *
   * @param ex MethodArgumentNotValidException
   * @param headers HttpHeaders
   * @param status HttpStatus
   * @param request WebRequest
   * @return ResponseEntity with exceptionResponse for bad request
   * @author Pabasara
   * @since 1.0
   */
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    List<String> errors =
        ex.getBindingResult().getFieldErrors().stream()
            .map(messageSourceResolvable -> messageSourceResolvable.getDefaultMessage())
            .collect(Collectors.toList());
    ApplicationExceptionResponse exceptionResponse =
        new ApplicationExceptionResponse(
            errors,
            ZonedDateTime.now(),
            request.getDescription(false),
            HttpStatus.BAD_REQUEST.value());
    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }
}
