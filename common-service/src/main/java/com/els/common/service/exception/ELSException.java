package com.els.common.service.exception;

/**
 * The ELSException class is used to throw exceptions when general exceptions occurred.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public class ELSException extends RuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = -3098669988509514800L;

  /** module error code */
  private String moduleErrorCode;

  /**
   * Initializes message.
   *
   * @param message String
   * @author Pabasara
   * @since 1.0
   */
  public ELSException(String message) {
    super(message);
  }

  /**
   * Initializes message and cause.
   *
   * @param message String
   * @param cause cause for the exception
   * @author Pabasara
   * @since 1.0
   */
  public ELSException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Initializes message and moduleErrorCode.
   *
   * @param message String
   * @param moduleErrorCode message specified module error code
   * @author Pabasara
   * @since 1.0
   */
  public ELSException(String message, String moduleErrorCode) {
    super(message);
    this.moduleErrorCode = moduleErrorCode;
  }

  /**
   * Gets module error code.
   *
   * @return moduleErrorCode String
   * @author Pabasara
   * @since 1.0
   */
  public String getModuleErrorCode() {
    return moduleErrorCode;
  }
}
