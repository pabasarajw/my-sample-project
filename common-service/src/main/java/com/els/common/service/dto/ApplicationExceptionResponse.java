package com.els.common.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * The ApplicationExceptionResponse class will be used to respond exceptions occurring in
 * application.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationExceptionResponse implements Serializable {

  /** errorMessage */
  private Object errorMessage;

  /** timestamp */
  private ZonedDateTime timestamp;

  /** description of error */
  private String description;

  /** error return code. */
  private int returnCode;
}
