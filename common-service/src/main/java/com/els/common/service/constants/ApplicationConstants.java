package com.els.common.service.constants;

/**
 * The ApplicationConstants class is used to define common constants used across Microservices.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public class ApplicationConstants {

  /** Unknown Error Message */
  public static final String UNKNOWN_ERROR_MESSAGE = "An unknown error has occurred.";

  /** Resource Not Found Error Message */
  public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE = "Resource not found.";

  /** Bad Request Error Message */
  public static final String BAD_REQUEST_ERROR_MESSAGE = "Bad Request.";
}
