package com.els.common.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * This class is used to initialize RedisConnection and RedisTemplate.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
@Configuration
public class RedisConfig {

  /** Redis host name */
  @Value("${spring.redis.host}")
  private String redisHostName;

  /** Redis port */
  @Value("${spring.redis.port}")
  private int redisPort;

  /** Redis password */
  @Value("${spring.redis.password}")
  private String redisPassword;

  /**
   * This method is used to create a bean of JedisConnectionFactory.
   *
   * @return JedisConnectionFactory
   * @author Pabasara
   * @since 1.0
   */
  @Bean
  JedisConnectionFactory jedisConnectionFactory() {
    String password = redisPassword;
    RedisPassword redisPassword = RedisPassword.of(password);
    RedisStandaloneConfiguration redisStandaloneConfiguration =
        new RedisStandaloneConfiguration(redisHostName, redisPort);
    redisStandaloneConfiguration.setPassword(redisPassword);
    return new JedisConnectionFactory(redisStandaloneConfiguration);
  }

  /**
   * This method is used to crate a bean of RedisTemplate.
   *
   * @return RedisTemplate for BaseRedisData.
   * @author Pabasara
   * @since 1.0
   */
  @Bean(name = "RedisTemplate")
  public RedisTemplate<String, ?> RedisTemplate() {
    final RedisTemplate<String, ?> template = new RedisTemplate<>();
    template.setConnectionFactory(jedisConnectionFactory());
    template.setEnableTransactionSupport(true);
    template.setKeySerializer(new StringRedisSerializer());
    template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
    return template;
  }
}
