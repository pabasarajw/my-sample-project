# Installation Guide

First you need to build `common-service` and then you need to build and run `elibrary-service`.
- [Installation guide for common-service](https://gitlab.com/pabasarajw/my-sample-project/-/blob/main/common-service/README.md)
- [Installation guide for elibrary-service](https://gitlab.com/pabasarajw/my-sample-project/-/tree/main/elibrary-service/README.md)

